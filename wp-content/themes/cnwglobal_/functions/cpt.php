<?php
// add services cpt

add_action('init', 'services_init');
function services_init() {
    $labels = array(
        'name' => 'Services',
        'singular_name' => 'Service',
        'add_new' => 'Add new service',
        'add_new_item' => 'Add new service',
        'edit_item' => 'Edit service',
        'new_item' => 'New service',
        'view_item' => 'View service',
        'search_items' => 'Find service',
        'not_found' =>  'Not found',
        'not_found_in_trash' => 'Not found in trash',
        'parent_item_colon' => '',
        'menu_name' => 'Services'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,

        'rewrite' => array('slug' => 'services','with_front' => FALSE),
        // 'query_var' => "services",
        // 'hierarchical' => false,
        // '_builtin' => false,
        // 'has_archive' => true,
        // 'public'=>true,
    );
flush_rewrite_rules();
    register_post_type('services',$args);
    
}


// metabox
// function services_callback() {
//     add_meta_box( 'services_id', 'Services properties', 'services_meta_box_callback', $screen );
// }
/* HTML код блока */
// function services_meta_box_callback() {
//     // Используем nonce для верификации
//     wp_nonce_field( plugin_basename(__FILE__), 'myplugin_noncename' );

//     // Поля формы для введения данных
//     echo '<label for="myplugin_new_field">' . __("Description for this field", 'myplugin_textdomain' ) . '</label> ';
//     echo '<input type="text" id= "myplugin_new_field" name="myplugin_new_field" value="whatever" size="25" />';
// }
/* HTML код блока end*/

// add services cpt end

// add solutions cpt
add_action('init', 'solutions_init');
function solutions_init() {
    $labels = array(
        'name' => 'Solutions',
        'singular_name' => 'Solution',
        'add_new' => 'Add new solution',
        'add_new_item' => 'Add new solution',
        'edit_item' => 'Edit solution',
        'new_item' => 'New solution',
        'view_item' => 'View solution',
        'search_items' => 'Find solution',
        'not_found' =>  'Not found',
        'not_found_in_trash' => 'Not found in trash',
        'parent_item_colon' => '',
        'menu_name' => 'Solutions'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','editor','thumbnail')
    );
    register_post_type('solutions',$args);
}
// add solutions cpt end

// add industries cpt
add_action('init', 'industries_init');
function industries_init() {
    $labels = array(
        'name' => 'Industries',
        'singular_name' => 'Industry',
        'add_new' => 'Add new industry',
        'add_new_item' => 'Add new industry',
        'edit_item' => 'Edit industry',
        'new_item' => 'New industry',
        'view_item' => 'View industry',
        'search_items' => 'Find industry',
        'not_found' =>  'Not found',
        'not_found_in_trash' => 'Not found in trash',
        'parent_item_colon' => '',
        'menu_name' => 'Industries'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','editor','thumbnail')
    );
    register_post_type('industries',$args);
}
// add industries cpt end

// add about cpt
add_action('init', 'about_init');
function about_init() {
    $labels = array(
        'name' => 'About',
        'singular_name' => 'About',
        'add_new' => 'Add new About',
        'add_new_item' => 'Add new About',
        'edit_item' => 'Edit About',
        'new_item' => 'New About',
        'view_item' => 'View About',
        'search_items' => 'Find About',
        'not_found' =>  'Not found',
        'not_found_in_trash' => 'Not found in trash',
        'parent_item_colon' => '',
        'menu_name' => 'About us'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','editor','thumbnail')
    );
    register_post_type('about',$args);
}
// add about cpt end