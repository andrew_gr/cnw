 <?php
 /**
  * Template name: China template
  */
 ?>

<?php get_header() ?>

        <div class="top_slider_wrapper">
            <div id="top_slider">
                <?php
                foreach (get_field('china_slider','options') as $key => $value) {
                    $slider_img_link = $value['china_slider_img']['sizes']['slider-thumbs'];
                    $slider_title = $value['china_slider_ttl'];
                ?>
                    <div>
                        <img src="<?php echo $slider_img_link ?>"/>
                        <?php
                            echo "<div class='titles_wrapper china_slider_ttl'>".$slider_title."</div>";
                        ?>
                    </div>
                <?php
                }
                ?>
            </div>      
        </div>

        <div id="announce">
            <div class="container">
                <div class="row">
                <?php
                    $anounces = get_field('under_slider_block','options');
                    foreach ($anounces as $key => $value) {
                        
                ?>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="announce_item china_announce_item">
                            <h3><?php echo $value['china_b_ttl']; ?></h3>
                            <div class="announce_txt">
                                <?php echo $value['china_b_txt']; ?>
                            </div>
                        </div>
                    </div>
                <?php
                    }
                ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="china_search_wrapper">
                            <div class="announce_item announce_item_track china_search_wrap">
                                <form method="get" action="<?php echo get_field('track_trace_link','options') ?>">
                                    <input name="TrackingNumber" type="text" class="tracking_inp" />        
                                    <input name="return" type="hidden" value="http://www.cnwglobal.com" />        
                                    <input type="Submit" value="GO" class="tracking_sbm" />        
                                </form>
                            </div>
                            <p class="china_search_ttl"><?php echo get_field('china_tt', 'options') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="content" class="container">
            <div class="row">

                <div id="ch_industry ch_service">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ch_industry_item_wrap ch_service_item_wrap">
                        <h3><?php echo get_field('china_choose_ttl', 'options') ?></h3>
                    </div>
                    <?php
                        $choose_items = get_field('choose_items', 'options');
                        foreach ($choose_items as $key => $value) {
                    ?>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ch_industry_item_wrap">
                        <div class="ch_industry_item">
                            <img alt="b-semi" src="<?php echo $value['china_choose_img']['sizes']['services-thumbs']; ?>" />
                            <p class="ch_industry_item_ttl"><?php echo $value['china_choose_ttl'] ?></p>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
        <?php wp_footer(); ?>
        <footer class="china_footer container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="china_footer_left">
                        <div class="ch_fl_contact">
                            <span>联络我们</span>
                            <span><?php echo get_field('china_footer_left_title','options') ?></span>
                        </div>
                        <div class="china_contacts_wrap">
                            <div class="china_phone_wrap">
                                <p><span>中国：</span><span><?php echo get_field('china_phone1','options') ?></span></p>
                                <p><span>中国香港特别行政区：</span><span><?php echo get_field('china_phone2','options') ?></span></p>
                            </div>
                            <div class="china_email_wrap">
                                <p><?php echo get_field('china_email1','options') ?></p>
                                <p><?php echo get_field('china_email2','options') ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="ch_fr_txt">
                        <span><?php echo get_field('china_footer_right_text','options') ?></span>
                    </div>
                </div>
            </div>
        </footer>