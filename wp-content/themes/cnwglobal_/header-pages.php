<?php
    $cpost_type = $post->post_type;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php wp_head(); ?>
        <title><?php the_field('home_title','options'); ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon">
        <!-- <link rel="stylesheet" href="/css/front.css" type="text/css" /> -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="js/owl_carousel/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="js/owl_carousel/owl.theme.css">
        <link rel="stylesheet" type="text/css" href="style.css"> -->
    </head>
    <body <?php body_class() ?>>
        <div class="header_wrapper clearfix">
            <div id="header" class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div id="logo">
                            <a href="<?php bloginfo('url'); ?>">
                                <img src="<?php the_field('logo','options'); ?>" alt="Home" title="CNW" border="0" width="149" height="90"/>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                        <div id="nav">
                            <div id="navt" class="nav_wrap">
                                <?php 
                                    wp_nav_menu(array(
                                        'theme_location' => 'top_menu',
                                        'container' => '',
                                        'menu_class' => 'nav navbar-nav top-menu'
                                    )); 
                                ?>
                                <a class="china_flag" href="<?php the_field('china_page_link','options'); ?>"><img src="<?php the_field('china_flag','options'); ?>" alt="china"></a>
                            </div>
                            <div id="navb" class="nav_wrap">
                                <!-- <?php 
                                    wp_nav_menu(array(
                                        'theme_location' => 'main_menu',
                                        'container' => '',
                                        'menu_class' => 'nav navbar-nav main-menu nomobile_page_menu'
                                    )); 
                                ?> -->
                                <ul class="full_main_menu_wrap full_main_menu_wrap_page">
                                <?php
                                    $menu_items = wp_get_nav_menu_items("Main menu");
                                    wp_reset_query();
                                    $current_pages_id = $post->ID;

                                    foreach ($menu_items as $key => $value) {
                                        if ( 'page' != $value->object ) {
                                           
                                            echo "<li class='full_main_menu_item full_main_menu_front_item clearfix noactive_m'><a href='".$value->url."' class='full_main_menu_front_item_a ".$value->object."'>".$value->object."</a><ul class='inner_subitems_wrap'>";
                                            //
                                            $args = array(
                                                'orderby' => 'date',
                                                'order' => 'ASC',
                                                'post_type' => $value->object,
                                            );
                                            $wp_query = new WP_Query( $args );
                                            while ( $wp_query->have_posts() ) {
                                                $wp_query->the_post();
                                                $post_id = get_the_ID();
                                                echo "<li class='full_main_menu_inner_menu_item clerfix'><a href='".get_permalink($post_id)."'>".$post->post_title."</a></li>";
                                            }
                                            echo "</ul></li>";
                                        } else {
                                            print_r($value);
                                            if ($value->object_id == $current_pages_id) $active_menu_item = 'active_item_menu';
                                            echo "<li class='".$active_menu_item." full_main_menu_front_item'><a href='".$value->url."' class='full_main_menu_front_item_a'>".$value->title."</a></li>";
                                        }
                                    }
                                ?>
                                </ul>
                                <div id="phone">        
                                    <p><?php the_field('phones','options'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="subnav">
                <div class="container">
                 <nav id="main_mob_menu" class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse collapse_main" id="bs-example-navbar-collapse-2">
                        <div class="mobile_menu_wrap">
                            <ul id="accordion1" class="top_menu_wrap">
                            <?php
                                $menu_items = wp_get_nav_menu_items("Main menu");
                                foreach ($menu_items as $key => $value) {
                                    if ( 'page' != $value->object ) {
                                        echo "<li class='panel panel-default top_menu_item front_menu_items clearfix'><a class='front_item_a' role='button' data-parent='#accordion1' data-toggle='collapse' aria-expanded='false' href='#collapse".$key."' class='accord_btn'>".$value->object."</a><ul id='collapse".$key."' class='panel-collapse collapse'>";
                                        // 
                                        $args = array(
                                            'orderby' => 'date',
                                            'order' => 'ASC',
                                            'post_type' => $value->object,
                                        );
                                        $wp_query = new WP_Query( $args );
                                        while ( $wp_query->have_posts() ) {
                                            $wp_query->the_post();
                                            $post_id = get_the_ID();
                                            echo "<li class='inner_menu_item clerfix'><a href='".get_permalink($post_id)."'>".$post->post_title."</a></li>";
                                        }
                                        echo "</ul></li>";
                                    } else {
                                        echo "<li class='front_menu_items'><a href='".$value->url."' class='front_item_a'>".$value->title."</a></li>";
                                    }
                                }
                            ?>
                            </ul>
                        </div>
                    </div>
                </nav>
                </div>
            </div>
        </div>
        <?php wp_reset_query(); ?>