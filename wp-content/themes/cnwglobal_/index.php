<?php get_header() ?>

        <div class="top_slider_wrapper">
            <div id="top_slider">
                <?php
                    foreach (get_field('slider_fields','options') as $key => $value) {
                        $slider_link = $value['slider_link'];
                        $slider_img_link = $value['slider_img']['sizes']['slider-thumbs'];
                        $slider_titles = $value['slider_titles'];

                ?>
                        <a href="<?php echo $slider_link ?>">
                            <img src="<?php echo $slider_img_link ?>" alt="slide1" />
                            <ul class="titles_wrapper">
                                <?php
                                    if (!empty($slider_titles)) {
                                        foreach ($slider_titles as $key => $slider_title) {
                                            echo "<li>".$slider_title['slider_title']."</li>";
                                        }
                                    }
                                ?>
                                <?php
                                if (!empty($value['slider_link'])) {
                                ?>
                                        <button id="slider_btn" type="button" class="btn btn-default">Read more ></button>
                                <?php
                                    }
                                ?>
                            </ul>

                        </a>
                <?php
                    }
                ?>
            </div>      
        </div>

        <div id="announce">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="announce_item">
                            <h3><?php echo get_field('post_1_title','options') ?></h3>
                            <div class="announce_txt">
                                <?php echo get_field('post_1_text','options') ?>
                            </div>
                            <p class="announce_more">
                                <a href="
                                    <?php bloginfo('url'); ?>\<?php echo get_field('post_1','options')->post_type ?>\<?php echo get_field('post_1','options')->post_name ?>
                                ">
                                    <?php echo get_field('post_1_link_title','options') ?>
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="announce_item">
                            <h3><?php echo get_field('post_2_title','options') ?></h3>
                            <div class="announce_txt">
                                <?php echo get_field('post_2_text','options') ?>
                            </div>
                            <p class="announce_more">
                                <a href="
                                    <?php bloginfo('url'); ?>\<?php echo get_field('post_2','options')->post_type; ?>\<?php echo get_field('post_2','options')->post_name; ?>
                                ">
                                    <?php echo get_field('post_2_link_title','options') ?>
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="announce_item announce_item_track">
                            <form method="get" action="<?php echo get_field('track_trace_link','options') ?>">
                                <label for="Tracking" class="tracking_ttl"><?php echo get_field('track_trace_title','options') ?></label>
                                <input name="TrackingNumber" type="text" class="tracking_inp" />        
                                <input name="return" type="hidden" value="http://www.cnwglobal.com" />        
                                <input type="Submit" value="GO" class="tracking_sbm" />        
                            </form>
                            <a href="<?php echo get_field('track_trace_special_link','options') ?>" class="announce_link">
                                <img class="img-responsive" alt="hp-banner" src="<?php echo get_field('track_trace_banner','options') ?>"/>
                            </a>
                            <p class="announce_more">
                                <a href="#industry">Select your Industry</a>
                            </p>
                            <h3 class="track_under_ttl"><?php echo get_field('track_trace_under_ttl','options') ?></h3>
                            <div class="track_under_txt announce_txt"><?php echo get_field('track_trace_under_txt','options') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="content" class="container">
            <div class="row">

                <div id="ch_industry ch_service">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ch_industry_item_wrap ch_service_item_wrap">
                        <h3>CHOOSE YOUR SERVICE</h3>
                    </div>

                    <?php
                            wp_reset_query();
                            $args = array(
                                'orderby' => 'date',
                                'order' => 'ASC',
                                'post_type' => 'services'
                            );
                            //
                            $wp_query = new WP_Query( $args );
                            while ( $wp_query->have_posts() ) {
                                $wp_query->the_post();
                                $post_id = get_the_ID();
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ch_industry_item_wrap">
                                    <a class="ch_industry_item" href="<?php the_permalink() ?>" name="industry">
                                        <img alt="b-semi" src="<?php echo get_field('homepage_img', $post_id)['sizes']['services-thumbs']; ?>" />
                                        <p class="ch_industry_item_ttl"><?php the_title() ?></p>
                                    </a>
                                </div>
                                <?php
                            }
                        ?>
                </div>

                <div id="ch_industry">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ch_industry_item_wrap">
                        <h3>CHOOSE YOUR INDUSTRY</h3>
                    </div>

                    <?php
                            wp_reset_query();
                            $args = array(
                                'orderby' => 'date',
                                'order' => 'ASC',
                                'post_type' => 'industries'
                            );
                            //
                            $wp_query = new WP_Query( $args );
                            while ( $wp_query->have_posts() ) {
                                $wp_query->the_post();
                                $post_id = get_the_ID();
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ch_industry_item_wrap">
                                    <a class="ch_industry_item" href="<?php the_permalink() ?>" name="industry">
                                        <img alt="b-semi" src="<?php echo get_field('homepage_img', $post_id)['sizes']['services-thumbs']; ?>" />
                                        <p class="ch_industry_item_ttl"><?php the_title() ?></p>
                                    </a>
                                </div>
                                <?php
                            }
                        ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content_txt">
                    <?php
                        $choose_paragraphs = get_field('choose_paragraph','options');
                        foreach ($choose_paragraphs as $key => $choose_paragraph) {
                            echo "<h4>".$choose_paragraph['choose_paragraph_title']."</h4>";
                            echo $choose_paragraph_text = $choose_paragraph['choose_paragraph_text'];
                        }
                    ?>
                </div>
                
            </div>
        </div>

<?php get_footer(); ?>