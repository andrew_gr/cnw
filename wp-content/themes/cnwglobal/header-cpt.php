<?php
    $cpost_type = $post->post_type;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php wp_head(); ?>
        <title><?php the_field('home_title','options'); ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon">
        <!-- <link rel="stylesheet" href="/css/front.css" type="text/css" /> -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="js/owl_carousel/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="js/owl_carousel/owl.theme.css">
        <link rel="stylesheet" type="text/css" href="style.css"> -->
    </head>
    <body <?php body_class() ?>>
        <div class="header_wrapper clearfix">
            <div id="header" class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div id="logo">
                            <a href="<?php bloginfo('url'); ?>">
                                <img src="<?php the_field('logo','options'); ?>" alt="Home" title="CNW" border="0" width="149" height="90"/>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                        <div id="nav">
                            <div id="navt" class="nav_wrap">
                                <?php 
                                    wp_nav_menu(array(
                                        'theme_location' => 'top_menu',
                                        'container' => '',
                                        'menu_class' => 'nav navbar-nav top-menu'
                                    )); 
                                ?>
                                <a class="china_flag" href="<?php the_field('china_page_link','options'); ?>"><img src="<?php the_field('china_flag','options'); ?>" alt="china"></a>
                            </div>
                            <div id="navb" class="nav_wrap">
                                <!-- <?php 
                                    wp_nav_menu(array(
                                        'theme_location' => 'main_menu',
                                        'container' => '',
                                        'menu_class' => 'nav navbar-nav main-menu nomobile_main-menu'
                                    )); 
                                ?> -->
                                <ul class="full_main_menu_wrap full_main_menu_wrap_page">
                                <?php
                                    $menu_items = wp_get_nav_menu_items("Main menu");
                                    $current_page_id = $post->ID;
                                    $current_post_type = get_post_type($post->ID);

                                    foreach ($menu_items as $key => $value) {
                                        if ( 'page' != $value->object ) {
                                            if ($current_post_type == $value->object) {
                                                $menu_active = ' active_m';
                                            } else { $menu_active = ' noactive_m'; }
                                            // print_r( $value );
                                            echo "<li class='full_main_menu_item full_main_menu_front_item clearfix".$menu_active."'><a href='".$value->url."' class='full_main_menu_front_item_a ".$value->object."'>".$value->object."</a><ul class='inner_subitems_wrap'>";
                                            // 
                                            $args = array(
                                                'orderby' => 'date',
                                                'order' => 'ASC',
                                                'post_type' => $value->object,
                                            );
                                            $wp_query = new WP_Query( $args );
                                            while ( $wp_query->have_posts() ) {
                                                $wp_query->the_post();
                                                $post_id = get_the_ID();
                                                if ($current_page_id == $post_id) $current_menu_active = 'curr_active';
                                                echo "<li class='".$current_menu_active." full_main_menu_inner_menu_item clerfix'><a href='".get_permalink($post_id)."'>".$post->post_title."</a></li>";
                                            }
                                            echo "</ul></li>";
                                        } else {
                                            echo "<li class='full_main_menu_front_item'><a href='".$value->url."' class='full_main_menu_front_item_a'>".$value->title."</a></li>";
                                        }
                                    }
                                ?>
                                </ul>
                                <div id="phone">        
                                    <p><?php the_field('phones','options'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="subnav">
                <div class="container">
                    <!-- nomobile menu -->
                    <nav class="navbar navbar-default nomobile_inner_menu" role="navigation">
                        <!--  -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse collapse_main" id="bs-example-navbar-collapse-1">
                            <?php
                                //
                                global $wp_query;
                                wp_reset_query();
                                $current_post = $post->ID;
                                
                                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                // 
                                //
                                $args = array(
                                    // 
                                    'paged'=>$paged,
                                    'posts_per_page' => 12,
                                    'orderby' => 'date',
                                    'order' => 'ASC',
                                    //
                                    'post_type' => $cpost_type
                                );
                                //
                                echo "<ul>";
                                $wp_query = new WP_Query( $args );
                                while ( $wp_query->have_posts() ) {
                                    $wp_query->the_post();
                                    $post_id = get_the_ID();
                                    if ( $post_id == $current_post ) {
                                        $current_menu_item = "class='menu_active'";
                                    }
                                    echo "<li ".$current_menu_item."><a href='".get_permalink()."'>";
                                    $current_menu_item = "";
                                    the_title();
                                    echo "</a></li>";
                                }
                                echo "</ul>";
                            ?>
                        </div>
                    </nav>
                    <!-- nomobile menu end -->

                    <!-- mobile menu -->
                    <nav id="main_mob_menu" class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2mob">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse collapse_main1" id="bs-example-navbar-collapse-2mob">
                            <div class="mobile_menu_wrap">
                                <ul id="accordion1mob" class="nav navbar-nav top_menu_wrap_mob">
                                <?php
                                    $menu_items = wp_get_nav_menu_items("Main menu");
                                    foreach ($menu_items as $key => $value) {
                                        if ( 'page' != $value->object ) {
                                ?>
                                            <li class='dropdown top_menu_item front_menu_items'>
                                                <a href="<?php echo $value->url ?>" class="front_item_a mob_menu_item" ><?php echo $value->object ?></a>
                                                <a href="#" class="dropdown-toggle mob_menu_arrow" data-toggle="dropdown"><b class="caret"></b></a>    
                                                    <ul class='dropdown-menu'>
                                <?php
                                            $args = array(
                                                'orderby' => 'date',
                                                'order' => 'ASC',
                                                'post_type' => $value->object,
                                            );
                                            $wp_query = new WP_Query( $args );
                                            while ( $wp_query->have_posts() ) {
                                                $wp_query->the_post();
                                                $post_id = get_the_ID();
                                                echo "<li><a href='".get_permalink($post_id)."'>".$post->post_title."</a></li>";
                                            }
                                            echo "</ul></li>";
                                        } else {
                                            echo "<li class='front_menu_items'><a href='".$value->url."' class='front_item_a'>".$value->title."</a></li>";
                                        }
                                    }
                                ?>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <!-- mobile menu end -->

                </div>
            </div>
        </div>
        <?php wp_reset_query(); ?>