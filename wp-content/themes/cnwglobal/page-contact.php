<?php get_header('pages') ?>
<div class="inner_content_bg" style="background: url('<?php echo get_field('contact_bg')['url']; ?>') no-repeat; background-size: cover;"></div>
<div class="inner_content" style="z-index:2">
	<div class="container">
		<div class="inner_content_wrap">
			
            <h2 class="inner_content_ttl">
				<?php the_title() ?>
            </h2>
            <h4 class="inner_content_subttl">
				<?php echo get_field('contact_page_subtitle') ?>
            </h4>
            <div class="inner_content_txt">
				<p class="inner_content_quote_ttl inner_content_contact_ttl"><?php echo get_field('call_us_title') ?></p>
				<?php echo get_field('call_us_txt') ?>
				<p class="inner_content_quote_ttl inner_content_contact_ttl"><?php echo get_field('cnw_hot_title') ?></p>
				<?php echo get_field('cnw_hot_txt') ?>
				<a class="contacts_mail" href="mailto:info@cnwglobal.com"><?php echo get_field('contact_mail') ?></a>
				<div class="inner_form_wrap clearfix">
               		<?php echo do_shortcode( '[contact-form-7 id="170" title="Contact us form"]' ); ?>
				</div>
            </div>            
		</div>
	</div>
</div>

<?php get_footer('cpt'); ?>