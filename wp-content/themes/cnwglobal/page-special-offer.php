<?php get_header('pages') ?>
<div class="inner_content_bg" style="background: url('<?php echo get_field('contact_bg')['url']; ?>') no-repeat; background-size: cover;"></div>
<div class="inner_content" style="z-index:2">
	<div class="container">
		<div class="inner_content_wrap">
			
            <h2 class="inner_content_ttl">
				<?php echo get_field('title') ?>
            </h2>
            <h4 class="inner_content_subttl">
				<?php echo get_field('subtitle') ?>
            </h4>
            <div class="inner_content_txt">
				<?php echo get_field('description') ?>
				<div class="inner_form_wrap clearfix">
               		<?php echo do_shortcode( '[contact-form-7 id="270" title="Special offer"]' ); ?>
				</div>
            </div>            
		</div>
	</div>
</div>

<?php get_footer('cpt'); ?>