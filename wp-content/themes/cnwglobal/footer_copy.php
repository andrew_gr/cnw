        <div id="footer">
            <div class="container">
                
                <div id="footer_menu">
                    <?php 
                        wp_nav_menu(array(
                            'theme_location' => 'footer_main_menu',
                            'container' => '',
                            'menu_class' => 'nav navbar-nav footer_main_menu'
                        )); 
                    ?>
                </div>

                <div id="footer_social">       
                    <?php 
                        wp_nav_menu(array(
                            'theme_location' => 'footer_soc_menu',
                            'container' => '',
                            'menu_class' => 'nav navbar-nav footer_social_menu'
                        )); 
                    ?>
                </div>

                <div id="footer_copy">
                    <div>
                        <p class="footer_copy_txt"><?php the_field('copyright_text','options'); ?>.</p>
                        <?php 
                            wp_nav_menu(array(
                                'theme_location' => 'footer_bottom_menu',
                                'container' => '',
                                'menu_class' => 'nav navbar-nav footer_bottom_menu'
                            )); 
                        ?>
                    </div>
                </div>

            </div>
        </div>

        <!-- <script src="js/jquery-1.11.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl_carousel/owl.carousel.min.js"></script>
        <script src="js/main.js"></script> -->
        <?php wp_footer() ?>
    </body>
</html>