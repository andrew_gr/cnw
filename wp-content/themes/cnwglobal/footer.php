        <div id="footer">
            <div class="container">
                
                <div id="footer_menu">
                    <div class="footer_menu_b">
                        <div class="footer_menu_item_wrap">
                            <p class="footer_menu_ttl">Services</p>
                            <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'footer_menu_services',
                                    'container' => '',
                                    'menu_class' => 'nav navbar-nav footer_menu_item footer_menu_services'
                                ));
                            ?>
                        </div>
                        <div class="footer_menu_item_wrap">
                            <p class="footer_menu_ttl">Solutions</p>
                            <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'footer_menu_solutions',
                                    'container' => '',
                                    'menu_class' => 'nav navbar-nav footer_menu_item footer_menu_solutions'
                                ));
                            ?>
                        </div>
                    </div>
                    <div class="footer_menu_b">
                        <div class="footer_menu_item_wrap">
                            <p class="footer_menu_ttl">Industries</p>
                            <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'footer_menu_industries',
                                    'container' => '',
                                    'menu_class' => 'nav navbar-nav footer_menu_item footer_menu_industries'
                                ));
                            ?>
                        </div>
                        
                        <div class="footer_menu_item_wrap">
                            <p class="footer_menu_ttl">About</p>
                            <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'footer_menu_about',
                                    'container' => '',
                                    'menu_class' => 'nav navbar-nav footer_menu_item footer_menu_about'
                                ));
                            ?>
                        </div>
                    </div>
                </div>

                <div id="footer_social">       
                    <?php 
                        wp_nav_menu(array(
                            'theme_location' => 'footer_soc_menu',
                            'container' => '',
                            'menu_class' => 'nav navbar-nav footer_social_menu'
                        )); 
                    ?>
                </div>

                <div id="footer_copy">
                    <div>
                        <p class="footer_copy_txt"><?php the_field('copyright_text','options'); ?>.</p>
                        <?php 
                            wp_nav_menu(array(
                                'theme_location' => 'footer_bottom_menu',
                                'container' => '',
                                'menu_class' => 'nav navbar-nav footer_bottom_menu'
                            )); 
                        ?>
                    </div>
                </div>

            </div>
        </div>

        <!-- <script src="js/jquery-1.11.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl_carousel/owl.carousel.min.js"></script>
        <script src="js/main.js"></script> -->
        <?php wp_footer() ?>
    </body>
</html>