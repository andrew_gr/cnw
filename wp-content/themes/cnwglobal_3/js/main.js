jQuery(document).ready(function($) {
// jQuery(window).load(function() {

    $("#top_slider").owlCarousel({
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [979,1],
        itemsTablet: [768,3],
        itemsTablet: [618,1],
        itemsMobile : [420,1],
        items : 1,
        navigation : true,
        pagination : true,
        navigationText : ['','']
    });

    $('.full_main_menu_front_item').hover(
        function(){
            $('.nomobile_inner_menu').hide();
        },
        function(){
            $('.nomobile_inner_menu').show();
        }        
    );
    
});