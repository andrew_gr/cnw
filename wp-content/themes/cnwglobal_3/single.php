<?php get_header('cpt') ?>

<div class="inner_content" style="background: url('<?php echo get_field('background_image')['url']; ?>') no-repeat; background-attachment: fixed; background-size:cover">
    <div class="container">
        <div class="inner_content_wrap">
            <p class="announce_more inner_content_topmore">
                <a href="#quote">Get a quote from CNW</a>
            </p>
            <h2 class="inner_content_ttl">
                <?php the_title() ?>
            </h2>
            <h4 class="inner_content_subttl">
                <?php echo get_field('subtitle') ?>
            </h4>
            <div class="inner_content_txt clearfix">
                <?php the_content() ?>
                <p class="inner_content_quote_ttl">
                    <?php echo get_field('quote_title') ?>
                </p>
                <blockquote id="quote" class="inner_content_quote_txt">
                    <?php echo get_field('quote_text') ?>
                </blockquote>
                
            </div>

            <div class="inner_form_wrap clearfix">
                <p class="form_title">GET A QUOTE NOW</p>
                <?php echo do_shortcode( '[contact-form-7 id="170" title="Contact us form"]' ); ?>
            </div>

        </div>
    </div>
</div>

<?php get_footer('cpt'); ?>