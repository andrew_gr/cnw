<?php
/**
* register menu
**/
register_nav_menus(array(
    'top_menu' => 'Top menu',
));
register_nav_menus(array(
    'main_menu' => 'Main menu',
));
register_nav_menus(array(
    'footer_main_menu' => 'Footer main menu',
));
register_nav_menus(array(
    'footer_bottom_menu' => 'Footer bottom menu',
));
register_nav_menus(array(
    'footer_soc_menu' => 'Footer social menu',
));
/**
* Options page
*/
if( function_exists('acf_add_options_page') ) {    

    acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Common Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
    
    acf_add_options_sub_page(array(
        'page_title'    => 'Under slider section settings',
        'menu_title'    => 'Under slider section',
        'parent_slug'   => 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title'    => 'Choose section settings',
        'menu_title'    => 'Choose section',
        'parent_slug'   => 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title'    => 'Slider section settings',
        'menu_title'    => 'Slider section',
        'parent_slug'   => 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title'    => 'China page settings',
        'menu_title'    => 'China page',
        'parent_slug'   => 'theme-general-settings',
    ));

}
/**
* load scripts and styles
*/
add_action('wp_enqueue_scripts', 'load_style_script');
function load_style_script() {
    wp_enqueue_style('bootstrap_style', get_template_directory_uri() . '/css/bootstrap.css');
    wp_enqueue_style('owl_carousel_style', get_template_directory_uri() . '/js/owl_carousel/owl.carousel.css');
    wp_enqueue_style('owl_theme_style', get_template_directory_uri() . '/js/owl_carousel/owl.theme.css');
    wp_enqueue_style('custom_style', get_template_directory_uri() . '/style.css');

    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ));
    wp_enqueue_script('owl_carousel', get_template_directory_uri() . '/js/owl_carousel/owl.carousel.min.js', array( 'jquery' ));
    wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js');
}
/**
* enable post-thumbnails
**/
add_theme_support( 'post-thumbnails' );
add_image_size( 'services-thumbs', 310, 155, true );
add_image_size( 'services-thumbs_big', 720, 360, true );
add_image_size( 'slider-thumbs', 1600, 315, true );

require_once (TEMPLATEPATH . '/functions/cpt.php');