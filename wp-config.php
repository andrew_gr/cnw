<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cnwglobal_loc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[IWG`epcz.|lKn][CBpM)|uAP2OP5ht6D]$-8~w<I~1*,ReQ%^`ezOC>UI`oCG@4');
define('SECURE_AUTH_KEY',  'K$5_[*||V2;cZ/$S6HS4Z#UFfw~dnJVZg}x<j wH_tmL_|nuSGHc_4~VnFoyLLQV');
define('LOGGED_IN_KEY',    '{b/|Rviu5a[K2v4KwhU/r@fmMa6=qUcALbaJMyu/nC}e_3* pIAQt80SoaZ&Z#+y');
define('NONCE_KEY',        'BXo8B/&Fd$*IX3!@016Q(X!H&~g*{N)p^8p[iuQ6Bnx]u6jnn!RV9xP(lCk)LVLJ');
define('AUTH_SALT',        '9*G|sXR%]{2|E<-+aZ1VVPZ_@^Uj0q(GrZgY/T+C9rGh1TUFB(`&fC. g?xeU{Z/');
define('SECURE_AUTH_SALT', 'qD{lBpfZ@_=ISD I+J>LCJZXX1jOP47++?qORu!zK:o)gcbf}gN*WQjfc^1ZYNFy');
define('LOGGED_IN_SALT',   '%J`{KEaN3RjFlNk&J:HeP<|(*m)c]u}P)2h4;N9VIfnwuff:nio.&%nQ~$E]aCe4');
define('NONCE_SALT',       'AXx+y&I0QCNRLajnN=0KN]}Wjz`Kba#HBG`X1~RrD!a*r#,MKes)g/^G$cO:i[0~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cnw_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
